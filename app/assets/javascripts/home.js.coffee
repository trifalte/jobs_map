# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

#if $("html").is(".home")

  $(document).ready ->
    initialize = ->
      mapCanvas = $('div#map-canvas')[0]
      mapLatlng = new google.maps.LatLng(0, 0)
      mapOptions =
        zoom: 3
        center: mapLatlng
        mapTypeId: google.maps.MapTypeId.ROADMAP

      map = new google.maps.Map(mapCanvas, mapOptions)

      infowindow = new google.maps.InfoWindow
        content: "temp"
      markers = []
      i = 0

      for company, data of gon.marker_content
        myLatlng = new google.maps.LatLng(data["lat"], data["long"])
        markers[i++] = new google.maps.Marker
          position: myLatlng,
          map: map,
          title: company

      for marker in markers
        google.maps.event.addListener(marker, 'click', ->
                                                        infowindow.setContent(gon.marker_content[this.getTitle()]["info"])
                                                        infowindow.open(map, this))

    google.maps.event.addDomListener(window, 'load', initialize)