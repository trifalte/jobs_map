class HomeController < ApplicationController
  include HomeHelper
  def index
    @head_extras = '<!-- used for Google Maps API-->'\
    '<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />'\
    '<script type="text/javascript"'\
    "     src=\"https://maps.googleapis.com/maps/api/js?key=#{GoogleMaps::API_KEY}&sensor=false\">"\
      '</script>'.html_safe

    companies = Company.not_hidden.with_coordinates

    company_info = Hash.new

    companies.each do |company|
      company_info[company.name] = {"lat" => company.offices.first.lat,
                                    "long" => company.offices.first.long,
                                    "info" => generate_company_info(company)}
    end

    gon.marker_content = company_info
  end
end
