module HomeHelper
  def generate_company_info(company)
    info = '<div id="content">'+
    '<div id="siteNotice">'+
    '</div>'+
    "<h1 id=\"firstHeading\" class=\"firstHeading\">#{company.name}</h1>"+
    '<div id="bodyContent">'
    company.jobs.each do |job|
      info += '<p>'+
      "<h2><a href=\"#{company.angel_list_url}\">#{job.title}</a></h2>"+
      "<div class=\"salary_range\">$#{job.salary_min.to_s} - $#{job.salary_max.to_s}</div>"+
      "<div class=\"equity_range\">$#{job.equity_min.to_s} - $#{job.equity_max.to_s}</div>"+
      '</p>'
    end
    (info + '</div>'+ '</div>').html_safe
  end
end