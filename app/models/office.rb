class Office < ActiveRecord::Base
  belongs_to :company

  def address
    "#{address1}\n#{address2 + "\n" if !address2.blank?}"\
    "#{city}, #{state} #{zip_code}\n"\
    "#{country}"
  end
end
