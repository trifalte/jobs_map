class Company < ActiveRecord::Base
  has_many :jobs, dependent: :destroy
  has_many :offices, dependent: :destroy

  scope :hidden, -> { where('name is null') }
  scope :not_hidden, -> { where('name is not null') }
  scope :without_offices, -> { joins("LEFT JOIN offices on offices.company_id = companies.id").where('company_id is null').distinct }
  scope :with_offices, -> { joins("LEFT JOIN offices on offices.company_id = companies.id").where('company_id is not null').distinct }
  scope :with_coordinates, -> { joins(:offices).where('offices.lat is not null').distinct }
  scope :without_coordinates, -> { joins(:offices).where('offices.lat is null').distinct }

  def crunchbase_api_url
    crunchbase_url.sub(/www/, "api").sub(/company\//, "v/1/company/") + ".js?api_key=#{Crunchbase::API_KEY}"
  end
end
