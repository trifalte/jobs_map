class Job < ActiveRecord::Base
  belongs_to :company

  def save
    super
  rescue ActiveRecord::StatementInvalid => e
    if !e.message.match(/PG::NumericValueOutOfRange+/)
      raise e
    else
      logger.debug "Job with angel_list id #{attributes["angel_id"]} not saved, some integer too large"
    end
  end

end
