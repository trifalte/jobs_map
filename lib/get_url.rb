require 'net/http'

module GetUrl
  def get_api_data(url)
    response = get_url(url)
    # this should go in data_helper instead for the crunchbase specific stuff, possibly just use a yield here to check for 
    # api specific conditions
    # if response.code == "302"
    #       doc = parse_response(response, get_response_format(response))
    #       
    #     end
    parse_response(response, get_response_format(response))
  end

  def get_url(url)
    # build uri
    uri = URI.parse(URI.encode(url))

    # get the url
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    if url.match(/https:/)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    end
    http.request(request)
  end

  # TODO: write tests for this
  def parse_response(response, type)
    case type
    when :json
      JSON.parse(response.body)
    when :xml
      Nokogiri::XML(response.body)
    when :html
      Nokogiri::HTML(response.body)
    else
      response.body
    end
  end

  def get_response_format(response)
    case response["content-type"]
    when /json/
      :json
    when /xml/
      :xml
    when /html/
      :html
    else
      if response.body.first == "{"
        :json
      else
        :unknown
      end
    end
  end
end