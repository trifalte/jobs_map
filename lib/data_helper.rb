module DataHelper
  extend GetUrl

  def find_or_create_company_from_json(job)
    company = Company.where(angel_id: job["startup"]["id"]).first
    unless company
      company = Company.new(name: job["startup"]["name"], url: job["startup"]["company_url"],
                            angel_id: job["startup"]["id"], angel_list_url: job["startup"]["angellist_url"],
                            crunchbase_url: job["startup"]["crunchbase_url"])
      company.save
    end
    company
  end

  def find_update_or_create_job_from_json(job)
    existing_job = Job.where(angel_id: job["id"]).first
    if existing_job
      existing_job.touch
    else
      company = find_or_create_company_from_json(job)

      # Store the job
      # TODO: store tags for locations which might not be the main HQ
      # Also, store tags for skillsets asked for
      existing_job = Job.new(title: job["title"], company: company, angel_id: job["id"], salary_min: job["salary_min"],
                          salary_max: job["salary_max"], equity_min: job["equity_min"], equity_max: job["equity_max"])
    end
    existing_job.save if existing_job
    existing_job
  end

  def get_crunchbase_company_offices(company_data, company)
    return if company_data["offices"].blank?
    company_data["offices"].each do |office_data|
      company.offices.create!(description: office_data["description"], address1: office_data["address1"], address2: office_data["address2"],
                              zip_code: office_data["zip_code"], city: office_data["city"], state: office_data["state"],
                              country: office_data["country"], lat: office_data["latitude"], long: office_data["longitude"])
    end
  end
end