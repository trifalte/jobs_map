require 'get_url'
require 'data_helper'

namespace :data do
  include GetUrl
  include DataHelper

  desc "get jobs from angel list"
  task angel_list: :environment do
    time_start = Time.now
    p "Getting jobs started at #{time_start}"
    # Results are paginated so need to get the base url in order to determine how many pages to get
    jobs = get_api_data('https://api.angel.co/1/jobs')
    total_pages = jobs["last_page"].to_i

    (total_pages - 1).times do |page|
      time_start_page = Time.now
      # process the json file
      jobs["jobs"].each do |job|
        find_update_or_create_job_from_json(job)
      end
      # offset for next page and pagecount starts at 1 instead of 0
      jobs = get_api_data("https://api.angel.co/1/jobs?page=#{page + 2}")
      p "Page: #{jobs["page"]}, Time: #{Time.now - time_start_page}"
    end
    p "Getting jobs took #{Time.now - time_start}"
  end

  task crunchbase_url: :environment do
    time_start = Time.now
    p "Getting crunchbase_urls started at #{time_start}"
    angel_list_startup_api = "https://api.angel.co/1/startups/"
    companies = Company.not_hidden.where("crunchbase_url is null")
    unless companies.blank?
      companies.each do |company|
        time_start_page = Time.now
        company_detailed_data = get_api_data(angel_list_startup_api + company.angel_id.to_s)
        url = company_detailed_data["crunchbase_url"]
        p company_detailed_data
        company.crunchbase_url = url ? url : "not exist"
        company.save
        p "Company: #{company.name}, Time: #{Time.now - time_start_page}"
      end
    end
    p "Getting crunchbase_urls took #{Time.now - time_start}"
  end

  # consider removing jobs which don't exist anymore, not strictly necessary for MVP


  desc "match new companies with jobs to crunchbase"
  task crunchbase: :environment do
    time_start = Time.now
    # Pull all companies without offices listed on crunchbase
    companies = Company.not_hidden.without_offices.where("crunchbase_url is not null").where("crunchbase_url != 'not exist'")
    companies.each do |company|
      time_start_page = Time.now
      company_data = get_api_data(company.crunchbase_api_url)
      get_crunchbase_company_offices(company_data, company) if company_data
      p "Company: #{company.name}, Time: #{Time.now - time_start_page}"
    end
    p "Getting companies took #{Time.now - time_start}"
  end

end