require 'spec_helper'

describe DataHelper do
  class DummyClass
  end

  before(:each) do
    @dummy_class = DummyClass.new
    @dummy_class.extend(DataHelper)
  end

  describe '#find_or_create_company_from_json' do
    before(:all) do
      @existing_company = Company.new(angel_id: 32)
      @existing_company.save
    end

    it 'should find a company when it exists' do
      id, job = double(Hash), double(Hash)
      id.stub(:[]).with("id").and_return(32)
      job.stub(:[]).with("startup").and_return(id)
      expect(@dummy_class.find_or_create_company_from_json(job)).to eq(@existing_company)
    end

    it 'should create a company when it doesn\'t exist' do
      startup_hash = {"name" => "name", "company_url" => "company_url", "id" => 0,
                      "angel_list_url" =>"angel_list_url", "crunchbase_url" => "crunchbase_url"}
      job = double(Hash)
      job.stub(:[]).with("startup").and_return(startup_hash)
      returned_company = @dummy_class.find_or_create_company_from_json(job)
      expect(returned_company).not_to eq(@existing_company)
      expect(returned_company.angel_id).to eq(startup_hash["id"])
      expect(Company.where(angel_id: startup_hash["id"])).to exist
    end
  end

  describe '#find_update_or_create_job_from_json' do
    before(:all) do
      @existing_job = Job.new(angel_id: 32)
      @existing_job.save
    end

    it 'should find and update existing job' do
      job = double(Hash)
      job.stub(:[]).with("id").and_return(32)
      expect(@dummy_class.find_update_or_create_job_from_json(job)).to eq(@existing_job.reload)
    end

    it 'should create a new job if none found' do
      job = {"title" => "title", "startup"=> {"id" => 0}, "id" => 0, "salary_min" => 0,
            "salary_max" => 100, "equity_min" => 0.0, "equity_max" => 1.0}
      returned_job = @dummy_class.find_update_or_create_job_from_json(job)
      expect(returned_job).not_to eq(@existing_job)
      expect(returned_job.angel_id).to eq(job["id"])
      expect(Job.where(angel_id: job["id"])).to exist
    end
  end

  describe '#get_crunchbase_company_offices' do
    before(:all) do
      @company = Company.new
      @company.save!
      @company_offices = [{"description"=>"Headquarters", "address1"=>"1601 Willow Road", "address2"=>"",
                          "zip_code"=>"94025", "city"=>"Menlo Park", "state_code"=>"CA", "country_code"=>"USA",
                           "latitude"=>37.41605, "longitude"=>-122.151801},
                          {"description"=>"Europe HQ", "address1"=>"", "address2"=>"", "zip_code"=>"",
                          "city"=>"Dublin", "state_code"=>nil, "country_code"=>"IRL",
                          "latitude"=>53.344104, "longitude"=>-6.267494},
                          {"description"=>"New York", "address1"=>"340 Madison Ave", "address2"=>"",
                          "zip_code"=>"10017", "city"=>"New York", "state_code"=>"NY",
                          "country_code"=>"USA", "latitude"=>40.7557162, "longitude"=>-73.9792469}]
    end

    before(:each) do
      @company_data = double(Hash)
    end

    it 'should create a single office' do
      @company_data.stub(:[]).with("offices").and_return([@company_offices.first])
      expect{@dummy_class.get_crunchbase_company_offices(@company_data, @company)}.to change{Office.count}.by(1)
    end

    it 'should be able to create multiple offices' do
      @company_data.stub(:[]).with("offices").and_return(@company_offices)
      expect{@dummy_class.get_crunchbase_company_offices(@company_data, @company)}.to change{Office.count}.by(3)
    end

    it 'should do something sane if there are no offices' do
      @company_data.stub(:[]).with("offices").and_return(nil)
      expect{@dummy_class.get_crunchbase_company_offices(@company_data, @company)}.not_to change{Office.count}
    end
  end

  after(:all) do
    # Using delete is faster than destroy
    Company.delete_all
    Job.delete_all
    Office.delete_all
  end
end