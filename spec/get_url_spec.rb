require 'spec_helper'

describe GetUrl do
  class DummyClass
  end

  before(:each) do
    @dummy_class = DummyClass.new
    @dummy_class.extend(GetUrl)
    @base_url = "www.google.com"
  end

  describe '#get_url' do
    it 'succeeds without ssl' do
      VCR.use_cassette "lib/get_url/get_url_without_ssl" do
        url_without_ssh = "http://#{@base_url}"
        expect(@dummy_class.get_url(url_without_ssh).code).to eq("200")
      end
    end

    it 'succeeds with ssl' do
      VCR.use_cassette "lib/get_url/get_url_with_ssl" do
        url_without_ssh = "https://#{@base_url}"
        expect(@dummy_class.get_url(url_without_ssh).code).to eq("200")
      end
    end
  end

  describe '#get_response_format' do
    before(:each) do
      @response = double("HTTPResponse")
    end

    it 'correctly returns json when format is json' do
      @response.stub(:[]).with("content-type").and_return("application/json; charset=utf-8")
      expect(@dummy_class.get_response_format(@response)).to eq(:json)
    end

    it 'correctly returns xml when format is xml' do
      @response.stub(:[]).with("content-type").and_return("text/xml")
      expect(@dummy_class.get_response_format(@response)).to eq(:xml)
    end

    it 'correctly returns html when format is html' do
      @response.stub(:[]).with("content-type").and_return("text/html")
      expect(@dummy_class.get_response_format(@response)).to eq(:html)
    end

    it 'correctly returns json when format not json but first character is {' do
      @response.stub(:[]).with("content-type").and_return("text/something; charset=utf-8")
      @response.stub(:body => "{}")
      expect(@dummy_class.get_response_format(@response)).to eq(:json)
    end

    it 'returns unknown for all others' do
      @response.stub(:[]).with("content-type").and_return("text/css")
      @response.stub(:body => "l")
      expect(@dummy_class.get_response_format(@response)).to eq(:unknown)
    end
  end

  describe '#get_api_data' do
    # Angel list has a json api implementation
    it 'correctly returns hash file when url gives json as hash' do
      VCR.use_cassette "lib/get_url/json_response" do
        json_url = "https://api.angel.co/1/jobs"
        doc = @dummy_class.get_api_data(json_url)
        doc.should be_kind_of(Hash)
      end
    end

    # Crunchbase has an xml api implementation
    it 'will return xml file url gives xml' do
      VCR.use_cassette "lib/get_url/xml_response" do
        xml_url = "http://api.crunchbase.com/v/1/company/dw.js?api_key="
        doc = @dummy_class.get_api_data(xml_url)
        doc.should be_kind_of(Nokogiri::XML::Document)
      end
    end

    # Pretend Crunchbase has an unknown implementation
    it 'will return string when unknown type returned' do
      VCR.use_cassette "lib/get_url/unknown_response" do
        unknown_url = "http://api.crunchbase.com/v/1/company/dw.js?api_key="
        doc = @dummy_class.get_api_data(unknown_url)
        doc.should be_kind_of(String)
      end
    end

    # it 'will take care of grabbing redirected url in apis' do
    #       VCR.use_cassette "lib/get_url/redirected" do
    #         unknown_url = "http://api.crunchbase.com/v/1/company/dw.js?api_key="
    #         doc = @dummy_class.get_api_data(unknown_url)
    #         doc.should be_kind_of(String)
    #       end
    #     end
  end
end