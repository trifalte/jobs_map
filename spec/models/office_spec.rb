require 'spec_helper'

describe Office do
  before(:each) do
    @office = Office.new(description: "Headquarters", address1: "123 Main St.", address2: "",
                            zip_code: '12345', city: "City", state: "State",
                            country: "Country", lat: 23.1, long: 34.5)
  end

  describe '#address' do
    it 'should return a string with the formatted full address' do
      pretty_address = "123 Main St.\nCity, State 12345\nCountry"
      expect(@office.address).to eq(pretty_address)
    end
  end
end
