require 'spec_helper'

describe Company do
  before(:each) do
    @company = Company.new
  end

  describe '#crunchbase_api_url' do
    it 'should change www to api' do
      @company.crunchbase_url = "http://www.crunchbase.com/company/facebook"
      api_begin = "http://api"
      expect(@company.crunchbase_api_url).to start_with(api_begin)
    end

    it 'should add the v/1 namespace only before the company namespace' do
      @company.crunchbase_url = "http://www.crunchbase.com/company/facebook-company"
      api_url = "crunchbase.com/v/1/company/facebook-company"
      expect(@company.crunchbase_api_url).to include(api_url)
    end

    it 'should add .js to the end of crunchbase url' do
      @company.crunchbase_url = "http://www.crunchbase.com/company/facebook"
      api_js = ".js"
      expect(@company.crunchbase_api_url).to include(api_js)
    end
  
    it 'should add the crunchbase api key to the end' do
      @company.crunchbase_url = "http://www.crunchbase.com/company/facebook"
      api_end = "?api_key=#{Crunchbase::API_KEY}"
      expect(@company.crunchbase_api_url).to end_with(api_end)
    end
  end
end
