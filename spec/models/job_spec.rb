require 'spec_helper'

describe Job do
  describe '#rescue_integer_error_save' do
    it 'should rescue integer too big error for a job from postgres' do
      expect{Job.new(salary_max: 10000000000).save}.not_to raise_error
    end
  end
end
