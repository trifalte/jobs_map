class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :crunchbase_url
      t.string :angel_list_url
      t.string :url
      t.integer :angel_id
      t.integer :crunchbase_url

      t.timestamps
    end
  end
end
