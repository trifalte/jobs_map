class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :title
      t.integer :company_id
      t.integer :angel_id
      t.integer :salary_min
      t.integer :salary_max
      t.integer :equity_min
      t.integer :equity_max

      t.timestamps
    end
  end
end
