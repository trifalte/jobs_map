class ChangeJobEquityToFloat < ActiveRecord::Migration
  def change
    change_table :jobs do |t|
      t.remove :equity_min, :equity_max
      t.float :equity_min
      t.float :equity_max
    end
  end
end
