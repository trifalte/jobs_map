class ChangeCompanyCrunchbaseUrlToString < ActiveRecord::Migration
  def change
    change_table :companies do |t|
      t.remove :crunchbase_url
      t.string :crunchbase_url
    end
  end
end
