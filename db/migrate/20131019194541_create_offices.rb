class CreateOffices < ActiveRecord::Migration
  def change
    create_table :offices do |t|
      t.string :description
      t.integer :company_id
      t.string :address1
      t.string :address2
      t.string :zip_code
      t.string :city
      t.string :state
      t.string :country
      t.float :lat
      t.float :long

      t.timestamps
    end
  end
end
